# FlexKB

A theme for [Kanboard](https://github.com/kanboard/kanboard), an open source kanban project management board. The theme is based on the Github theme of the [Customizer plugin](https://github.com/creecros/Customizer). The Github theme itself is based on the [Moon theme](https://github.com/kenlog/Moon).

## Installation

You need to install the Customizer plugin for Kanboard first. After the installation of the plugin, upload the file `FlexKB.css` to the directory `plugins/Customizer/Assets/css` inside the directory, where you installed Kanboard.

Login as admin of the Kanboard, go to the settings and open the settings of the Customizer. Under the option "Manage Themes" you can select the theme FlexKB. Save your selection and you are done.

